<template>
  <!-- Header -->
  <div class="fixed inset-x-0 top-0 z-50 p-4 bg-white shadow">
    <div class="container px-4 mx-auto">
      <div class="flex gap-3">
        <button class="text-sm" @click="openNewTab">New File</button>

        <label class="text-sm cursor-pointer">
          Open File
          <input type="file" hidden @change="openNewFile" />
        </label>

        <button
          v-if="uploadedFile.url"
          class="text-sm"
          @click="showModal = true"
        >
          Save
        </button>

        <!-- Space -->
        <div class="ml-auto"></div>

        <button
          v-if="uploadedFile.url"
          class="text-sm"
          @click="drawSpectrogram"
        >
          Draw spectrogram
        </button>
      </div>
    </div>
  </div>

  <!-- Results -->
  <div class="container px-4 pt-16 pb-24 mx-auto">
    <!-- Audio container -->
    <div id="audio-container" class="my-4 bg-gray-100 rounded"></div>

    <!-- Time Line -->
    <div id="timeline"></div>

    <!-- Spectrogram -->
    <div v-show="statusSpectrogram" class="p-4 border rounded mt-14">
      <p class="mb-5 text-center">spectrogram</p>
      <div id="wave-spectrogram"></div>
    </div>
  </div>

  <!-- Footer -->
  <div class="fixed inset-x-0 bottom-0 z-50 p-4 bg-gray-50">
    <div class="container mx-auto">
      <div class="grid grid-cols-12 gap-4">
        <div class="col-span-3">
          <div class="flex items-center gap-3">
            <div>
              <!-- File name -->
              <p v-if="uploadedFile.name" class="text-sm">
                <span class="text-gray-500"> file name: </span>
                <span>{{ uploadedFile.name }}</span>
              </p>

              <!-- Duration -->
              <p v-if="uploadedFile.url" class="text-sm">
                <span class="text-gray-500"> duration: </span>
                <span>{{ currentTime }}</span>
              </p>
            </div>
          </div>
        </div>

        <div class="col-span-6">
          <div class="flex items-center justify-center h-full gap-3">
            <!-- Play and Stop -->
            <button v-if="uploadedFile.url" @click="togglePlay">
              <playPauseSVG class="w-6 text-blue-500" />
            </button>

            <!-- Record and stop -->
            <button @click="handleRecord">
              <span class="relative flex w-5 h-5">
                <span
                  v-if="recordStatus.recording && !recordStatus.pause"
                  class="absolute inline-flex w-full h-full bg-red-700 rounded-full opacity-75 animate-ping"
                ></span>
                <microphoneSVG
                  class="relative inline-flex w-5 h-5 text-red-500"
                />
              </span>
            </button>

            <!-- Record pause and resume -->
            <button v-if="recordStatus.recording" @click="handleRecordPause">
              <playPauseSVG class="w-5 text-red-500" />
            </button>
          </div>
        </div>

        <div class="col-span-3">
          <div
            v-if="uploadedFile.url"
            class="flex items-center justify-center h-full gap-x-2"
          >
            <button @click="changeZoom('zoom-out')">
              <zoomOutSVG class="w-4 text-blue-500" />
            </button>
            <input
              type="range"
              v-model.number="rangeSlider.val"
              :min="rangeSlider.min"
              :max="rangeSlider.max"
              class="w-44"
            />
            <button @click="changeZoom('zoom-in')">
              <zoomInSVG class="w-4 text-blue-500" />
            </button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <BaseModal v-if="showModal" @close="showModal = false">
    <template #header>
      <div class="flex items-center justify-between">
        <p>Enter a name</p>

        <button @click="showModal = false">
          <closeSVG />
        </button>
      </div>
    </template>

    <template #body>
      <div class="my-5">
        <input
          type="text"
          v-model="uploadedFile.name"
          class="px-2 py-1 border rounded"
        />
      </div>
    </template>

    <template #footer>
      <div>
        <button
          class="px-2 py-1 border rounded"
          v-if="uploadedFile.name"
          @click="saveFile"
        >
          Save
        </button>
      </div>
    </template>
  </BaseModal>
</template>

<script setup>
import { onMounted, reactive, ref, watch } from "vue";

import BaseModal from "@/components/BaseModal.vue";

import WaveSurfer from "wavesurfer.js";
import TimelinePlugin from "wavesurfer.js/dist/plugin/wavesurfer.timeline.min.js";
import CursorPlugin from "wavesurfer.js/dist/plugin/wavesurfer.cursor.min.js";
import MicrophonePlugin from "wavesurfer.js/dist/plugin/wavesurfer.microphone.min.js";
import SpectrogramPlugin from "wavesurfer.js/dist/plugin/wavesurfer.spectrogram.min.js";

import playPauseSVG from "@/assets/icons/play-pause.svg";
import microphoneSVG from "@/assets/icons/microphone.svg";

import zoomInSVG from "@/assets/icons/zoom-in.svg";
import zoomOutSVG from "@/assets/icons/zoom-out.svg";
import closeSVG from "@/assets/icons/close.svg";

let wavesurfer;
let mediaRecorder;

// All Data
const showModal = ref(false);
const statusSpectrogram = ref(false);
const currentTime = ref();

const uploadedFile = reactive({
  name: "",
  url: "",
});

const rangeSlider = reactive({
  val: 0,
  min: 0,
  max: 1000,
});

const recordStatus = reactive({
  recording: false,
  pause: false,
});

const waveParams = reactive({
  container: "#audio-container",
  backend: "WebAudio",
  responsive: true,

  plugins: [
    MicrophonePlugin.create({
      bufferSize: 16384,
      numberOfInputChannels: 1,
      numberOfOutputChannels: 1,
      constraints: {
        video: false,
        audio: true,
      },
    }),

    // SpectrogramPlugin.create({
    //   wavesurfer: wavesurfer,
    //   container: "#wave-spectrogram",
    //   labels: true,
    //   height: 256,
    // }),

    TimelinePlugin.create({
      container: "#timeline",
      dragSelection: false,
    }),

    CursorPlugin.create({
      showTime: true,
      opacity: 1,
      customShowTimeStyle: {
        "background-color": "#000",
        color: "#fff",
        padding: "2px",
        "font-size": "10px",
      },
    }),
  ],
});

// All watches
watch(
  () => rangeSlider.val,
  (val) => {
    wavesurfer.zoom(val);
  }
);

// All methods
const togglePlay = () => {
  wavesurfer.playPause();
};

const drawSpectrogram = () => {
  statusSpectrogram.value = true;

  wavesurfer
    .addPlugin(
      SpectrogramPlugin.create({
        wavesurfer: wavesurfer,
        container: "#wave-spectrogram",
        labels: true,
        height: 256,
        fftSamples: 1024,
      })
    )
    .initPlugin("spectrogram");
};

const changeZoom = (type) => {
  if (type === "zoom-in" && rangeSlider.val < rangeSlider.max) {
    rangeSlider.val += 50;
  } else if (type === "zoom-out" && rangeSlider.val > rangeSlider.min) {
    rangeSlider.val -= 50;
  }
};

const handleRecord = () => {
  if (!recordStatus.recording) {
    recordStatus.recording = true;

    wavesurfer.microphone.start();
  } else {
    recordStatus.recording = false;

    wavesurfer.microphone.stop();
    mediaRecorder.stop();
  }
};

const handleRecordPause = () => {
  if (!recordStatus.pause) {
    recordStatus.pause = true;

    mediaRecorder.pause();
    wavesurfer.microphone.pause();
  } else {
    recordStatus.pause = false;

    mediaRecorder.resume();
    wavesurfer.microphone.play();
  }
};

const storeAudio = (stream) => {
  mediaRecorder = new MediaRecorder(stream);
  mediaRecorder.start();

  const audioChunks = [];
  mediaRecorder.addEventListener("dataavailable", (event) => {
    audioChunks.push(event.data);
  });

  mediaRecorder.addEventListener("pause", () => {});

  mediaRecorder.addEventListener("stop", () => {
    const audioBlob = new Blob(audioChunks);
    const audioUrl = URL.createObjectURL(audioBlob);
    wavesurfer.load(audioUrl);

    uploadedFile.url = audioUrl;
    uploadedFile.name = "new-record";
  });
};

const openNewTab = () => {
  window.open(window.location.href, "_blank").focus();
};

const openNewFile = (e) => {
  const file = e.target.files[0];
  const audioUrl = URL.createObjectURL(file);

  uploadedFile.name = file.name;
  uploadedFile.url = audioUrl;

  wavesurfer.load(audioUrl);
};

const saveFile = () => {
  const aTag = document.createElement("a");

  aTag.href = uploadedFile.url;
  aTag.download = uploadedFile.name;

  document.body.appendChild(aTag);
  aTag.click();
  document.body.removeChild(aTag);

  showModal.value = false;
};

const updateTime = (time) => {
  currentTime.value = secondsToTimestamp(time);
};

const secondsToTimestamp = (seconds) => {
  seconds = Math.floor(seconds);
  var h = Math.floor(seconds / 3600);
  var m = Math.floor((seconds - h * 3600) / 60);
  var s = seconds - h * 3600 - m * 60;

  h = h < 10 ? "0" + h : h;
  m = m < 10 ? "0" + m : m;
  s = s < 10 ? "0" + s : s;

  return h + ":" + m + ":" + s;
};

//
//
//
//
//
//
//
//
//
// ***************** Mounted *****************
onMounted(() => {
  createAudio();
  waveLogs();
});

// ***************** Create *****************
const createAudio = () => {
  wavesurfer = new WaveSurfer(waveParams);
  wavesurfer.init();
};

// ***************** Logs *****************
const waveLogs = () => {
  wavesurfer.on("ready", function () {
    console.warn("ready...");

    updateTime(wavesurfer.getCurrentTime());
  });

  wavesurfer.microphone.on("deviceReady", function (stream) {
    console.info("Device ready!");

    storeAudio(stream);
  });

  wavesurfer.microphone.on("deviceError", function (code) {
    console.warn("Device error: " + code);
  });

  wavesurfer.on("audioprocess", function () {
    console.warn("audioprocess...");

    updateTime(wavesurfer.getCurrentTime());
  });

  wavesurfer.on("seek", function () {
    console.warn("seek...");

    updateTime(wavesurfer.getCurrentTime());
  });

  wavesurfer.on("finish", function () {
    console.log("finish");
  });

  wavesurfer.on("error", function (e) {
    console.warn("error...");
    console.warn(e);
  });
};
</script>
